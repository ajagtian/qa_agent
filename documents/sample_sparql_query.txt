Queries in the paper won't work as they are designed for old DBPEDIA..
These sample queries work, try them

#Query on:
http://dbpedia.org/sparql


#get all the ontologies

select * where
{
<http://dbpedia.org/resource/Karakoram> ?Property?h.
}



#query on suitable ontology

select distinct * WHERE
{
<http://dbpedia.org/resource/Karakoram>
<http://dbpedia.org/property/highest> ?v .

}
