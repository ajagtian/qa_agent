#!/usr/bin/python2.7 -tt


from flask import Flask, render_template
import answer

app = Flask(__name__)


@app.route('/')
def	it_works():
		return render_template('index.html', name=None)

@app.route('/answer/<question>')
def get_answer(question=None):
	args = []
	args.append(question)
	json = answer.main(args)
	return unicode(str(json), 'utf-8')


if __name__ == '__main__':
	app.debug = True
	app.run(host='0.0.0.0')

	
